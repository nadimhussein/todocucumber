Feature: Add Todo
  Scenario: Create todo by POST /todos
    Given ensure rest endpoint of "34.71.130.53:9010" is up
    When a POST request to /todos is made
    And the request body is
      """
{
  "targetDate": "2020-03-03",
  "description": "Test",
  "user": "Ajay",
  "done": false
}
      """
    Then a 201 response is returned within 2000ms